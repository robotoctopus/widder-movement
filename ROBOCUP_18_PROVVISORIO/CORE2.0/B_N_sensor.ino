#include "QTRSensors.h"
 
/*B_N_sensor*/
const int numSens = 8;
const int timeOut = 2500;
const int pinEmitter = 33;

QTRSensorsRC qtrrc((unsigned char[]) {41, 37, 43, 51, 49, 45, 47, 39}, numSens, timeOut, pinEmitter); //define pin of BW sensor
unsigned int sensor[8];      //define pin array 

void calibration() {
  for (int i = 0; i < 400; i++) {      // minimo 400
    qtrrc.calibrate();
  }
}

void  READ_SBN () {                                    
  qtrrc.readCalibrated(sensor);
}


#define sio 10      // ColorPAL connected to pin 2
#define unused  255    // Non-existant pin # for SoftwareSerial
#define sioBaud 4800
#define waitDelay 200

SoftwareSerial serin(sio, unused);
SoftwareSerial serout(unused, sio);


void setPAL(){
  reset();          // Send reset to ColorPal
  serout.begin(sioBaud);
  pinMode(sio, OUTPUT);
  serout.print("= (00 $ m) !"); // Loop print values, see ColorPAL documentation
  serout.end();       // Discontinue serial port for transmitting

  serin.begin(sioBaud);         // Set up serial port for receiving
  pinMode(sio, INPUT);
 
}

  
void green() {
 float X = readData();
 
 if (X == 1.){
 // Serial.print("PARTO");
  destra(1000);
  }
}  

// Reset ColorPAL; see ColorPAL documentation for sequence
void reset() {
  delay(200);
  pinMode(sio, OUTPUT);
  digitalWrite(sio, LOW);
  pinMode(sio, INPUT);
  while (digitalRead(sio) != HIGH);
  pinMode(sio, OUTPUT);
  digitalWrite(sio, LOW);
  delay(80);
  pinMode(sio, INPUT);
  delay(waitDelay);
}

float readData() {
  char buffer[32];
  
  if (serin.available() > 0) {
    // Wait for a $ character, then read three 3 digit hex numbers
    buffer[0] = serin.read();
    if (buffer[0] == '$') {
      for(int i = 0; i < 9; i++) {
        while (serin.available() == 0);     // Wait for next input character
        buffer[i] = serin.read();
        if (buffer[i] == '$')               // Return early if $ character encountered
          return;
      }
     float X = parseAndPrint(buffer);
     return X; 
    }
  }
}

// Parse the hex data into integers
float parseAndPrint(char * data) {
  int red;
  int grn;
  int blu;
  char verde[32];
  char rosso;
  sscanf (data, "%3x%3x%3x", &red, &grn, &blu);
  char buffer[32];
  
  sprintf(buffer, "%4.4d", blu);
  sprintf(verde, "%4.4d", grn);

  String testV = verde;
  float V = testV.toFloat(); 
  
  String test = buffer;
  float B = test.toFloat(); 

  float X = 0.;
/*  Serial.print(V);
  Serial.print(" ");
  Serial.println(B);*/

  if( B > 50 && B < 70 && V > 45 && V < 62)  X= 1.;
   return X;
}

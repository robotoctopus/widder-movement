#include "QTRSensors.h"

/*#define velZ   30
#define velA   80     // calibrated values
#define velB   85         
#define velC   90
#define velD   95
#define velE  100
#define velF  103
#define velG  120 */

#define vel  30  // velocità per andare dritti
#define deltaH 40
#define deltaL 85
float var;

float c = 60;

float distanza(){
  //int t = millis();
  float n = 0;
  float d = 0;
  float x = 0;
  
  READ_SBN();               // lettura sensori B_N
  
  n = sensor[0]*-7.0 + sensor[1]*-5.0 + sensor[2]*-3.0 + sensor[3]*-1.0+ sensor[4]*1.0 + sensor[5]*3.0 + sensor[6]*5.0+ sensor[7]*7.0; // nominatore
  d = sensor[0] + sensor[1] + sensor[2] + sensor[3] + sensor[4] + sensor[5] + sensor[6] + sensor[7]; // denominatore
  
  if( d == 0 ) x = 0;
  else{
    x = n/d;
  }
  x = x/7.0;
  //Serial.print("----------------tempo impiegato per distanza: ");
  //int t2= millis();
  //Serial.println(t2 - t);
  return x;
}
 
void pina(){
  float x = distanza();
  //int t = millis();
  
    
  var = x*c;
  

  
  
  int L = vel + (int)var;  // migliorabile manca qualcosa (forse la mai calibrazione?)
  int R = vel - (int)var;  // migliorabile manca qualcosa
  //Serial.print("----------------tempo impiegato per pina: ");
  //int t2= millis();
  //Serial.println(t2 - t);
  motors(L,R);  
}

/*void sensorSimulation(float v) {
  float sensorVal = v * c;
  int L = vel + (int)sensorVal + 4;  // migliorabile manca qualcosa (forse la mai calibrazione?)
  int R = vel - (int)sensorVal - 4;  // migliorabile manca qualcosa
  motors(L,R);
}*/

void motors(int L, int R){
  //int t = millis();

  /* WARNING FOR DEBUG, THIS ADDS 15 MILLISECONDS DELAY!!!! */
  
  if( L > 255 ) L = 255;
  if( L < -255 ) L = -255;
  if( R > 255 ) R = 255;
  if( R < -255 ) R = -255;

  
  if (R < 0) {

    R= -R;
    int RV = (int) ((255-deltaL)*R/255)+deltaL;
    //RV = RV - 5;
    
    digitalWrite(28,LOW);
    analogWrite(4,RV+20);

    digitalWrite(24,LOW);  
    analogWrite(2,RV);
    
  
  } else {
    
   int RV = (int) ((255-deltaH)*R/255)+deltaH;
    RV = RV - 5;
        
    digitalWrite(28,HIGH);
    analogWrite(4,RV);

    digitalWrite(24,HIGH);
    analogWrite(2,RV);
    
  }

  if (L < 0) {
    
    L=-L;
    int LV = (int) ((255-deltaL)*L/255)+deltaL;
    LV = LV + 6;
    
    digitalWrite(26,LOW);
    analogWrite(3,LV);
 
    digitalWrite(22,LOW);
    analogWrite(5,LV);
    
    
  } else {

    int LV = (int) ((255-deltaH)*L/255)+deltaH;
    LV = LV + 6;
    
    digitalWrite(26,HIGH);
    analogWrite(3,LV);
 
    digitalWrite(22,HIGH);
    analogWrite(5,LV);
    
  }
  //Serial.print("----------------tempo impiegato per motori: ");
  //int t2= millis();
  //Serial.println(t2 - t);
}



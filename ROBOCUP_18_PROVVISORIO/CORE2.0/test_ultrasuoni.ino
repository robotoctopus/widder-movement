#define soglia  9

void trig(){
  digitalWrite(trigger, LOW);
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);
}

long values(){
  long durata = pulseIn(echo, HIGH);
  long distanza = 0.0343 * durata / 2;
  return distanza;
}

void Rev(int t){
  digitalWrite(28,LOW);
  analogWrite(4,60);

  digitalWrite(24,LOW);
  analogWrite(2,60);

  digitalWrite(26,LOW);
  analogWrite(3,60);
 
  digitalWrite(22,LOW);
  analogWrite(5,60);

  delay(t);
}

void dritto(int t){
  digitalWrite(28,HIGH);
  analogWrite(4,60);

  digitalWrite(24,HIGH);
  analogWrite(2,60);

  digitalWrite(26,HIGH);
  analogWrite(3,60);
 
  digitalWrite(22,HIGH);
  analogWrite(5,60);

  delay(t);
}

void stopped(int t){
  digitalWrite(28,HIGH);
  analogWrite(4,0);

  digitalWrite(24,HIGH);
  analogWrite(2,0);

  digitalWrite(26,HIGH);
  analogWrite(3,0);
 
  digitalWrite(22,HIGH);
  analogWrite(5,0);

  delay(t);
}

void sinistra(int t){
  digitalWrite(28,HIGH);
  analogWrite(4,100);

  digitalWrite(24,HIGH);
  analogWrite(2,100);

  digitalWrite(26,LOW);
  analogWrite(3,100);
 
  digitalWrite(22,LOW);
  analogWrite(5,100);

  delay(t);
}

void destra(int t){
  digitalWrite(28,LOW);
  analogWrite(4,90);

  digitalWrite(24,LOW);
  analogWrite(2,90);

  digitalWrite(26,HIGH);
  analogWrite(3,150);
 
  digitalWrite(22,HIGH);
  analogWrite(5,150);

  delay(t);
}

void ostacolo() {
  trig();
  long dist = values();
  if (dist < soglia){
    Rev(300);
    sinistra(3200);
    dritto(900);
    destra(3000);
    dritto(800);// da regolare
  destra(3200);
  dritto (1700);
  sinistra(500);
  }

}

#include "math.h"
#include <SoftwareSerial.h>

/* IMPORTANT MESSAGES!!!! */

/* APPARENTLY EVERY 2 MILLIS() + PRINTLN GIVES ~50 MILLISECONDS DELAY */
/* B&W SENSOR DEBUG GIVES 100 MILLISECONDS DELAY!! */
/* MOTOR ACTIVATION DEBUG GIVES 15 MILLISECONDS DELAY */

#define trigger 12
#define echo    11


//bool done = false;
 
void setup() {
  /*movimento motori*/
  pinMode(4,OUTPUT);
  pinMode(28,OUTPUT);
  pinMode(3,OUTPUT);
  pinMode(26,OUTPUT);
  pinMode(2,OUTPUT);
  pinMode(24,OUTPUT); 
  pinMode(5,OUTPUT);
  pinMode(22,OUTPUT);
  pinMode(33,OUTPUT);
   pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);
  calibration();   // calibrazione sensore B_N
  Serial.begin(9600);
  Serial.println("Calibration complete");

  // PAL calibration
 setPAL();
}  


void loop() {
  //int t = millis();
  
  pina();
  ostacolo();
  //green();
 // Serial.println("ok");
  //delay(10);
  
  /*
    Serial.print("----------------tempo impiegato per un ciclo: ");
    int t2= millis();
    Serial.println(t2 - t);
  */
}

/* pin qtr sensor {41, 37, 43, 51, 49, 45, 47, 39}
 * 
 * valori tarati per andare dritti (da modificare con le batterie)
 * 22 -> 103
 * 24 -> 95
 * 26 -> 103
 * 28 -> 95
 * 
 * valori tarati per curvare (90° = 1950ms) SX
 * 22 -> 85
 * 24 -> 90
 * 26 -> 80
 * 28 -> 80
 * 
 * valori tarati per curvare (90° = 1400ms) DX
 * 22 -> 100
 * 24 -> 100
 * 26 -> 120
 * 28 -> 120
  */

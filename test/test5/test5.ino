#include "math.h"

#define vel 20    // velocità per andare dritti
#define deltaH 35
#define deltaL 0
float var;

float c = 60;


void setup() {
  pinMode(4,OUTPUT);
  pinMode(28,OUTPUT);
  pinMode(3,OUTPUT);
  pinMode(26,OUTPUT);
  pinMode(2,OUTPUT);
  pinMode(24,OUTPUT); 
  pinMode(5,OUTPUT);
  pinMode(22,OUTPUT);
  pinMode(33,OUTPUT);
  Serial.begin(9600);
}

void motors(int L, int R){
  
  if( L > 255 ) L = 255;
  if( L < -255 ) L = -255;
  if( R > 255 ) R = 255;
  if( R < -255 ) R = -255;

  
  if (R < 0) {

    R= -R;
    int RV = (int) ((255-deltaL)*R/255)+deltaL;
    RV = RV - 5;
    
    digitalWrite(28,LOW);
    analogWrite(4,RV);

    digitalWrite(24,LOW);  
    analogWrite(2,RV);

    
      Serial.println("R indietro");
      Serial.println(RV);
    
  
  } else {
   Serial.println(R); 
   int RV = (int) ((255-deltaH)*R/255)+deltaH;
    RV = RV - 5;
        
    digitalWrite(28,HIGH);
    analogWrite(4,RV);

    digitalWrite(24,HIGH);
    analogWrite(2,RV);

    
      Serial.println("R avanti");
      Serial.println(RV);
    
    
  }

  if (L < 0) {
    
    L=-L;
    int LV = (int) ((255-deltaL)*L/255)+deltaL;
    LV = LV + 6;
    
    digitalWrite(26,LOW);
    analogWrite(3,LV);
 
    digitalWrite(22,LOW);
    analogWrite(5,LV);

    
      Serial.println("L indietro");
      Serial.println(LV);
    
    
  } else {

    int LV = (int) ((255-deltaH)*L/255)+deltaH;
    LV = LV + 6;
    
    digitalWrite(26,HIGH);
    analogWrite(3,LV);
 
    digitalWrite(22,HIGH);
    analogWrite(5,LV);

    
      Serial.println("L avanti");
      Serial.println(LV);
    
  }
}

void loop() {
  for (int i = -255; i <= 255; i++) {
    int L = vel + i;
    int R = vel - i;  
    motors(L,R);
    delay(500);
  }
  for (int i = 255; i >= -255 + 2*vel; i++) {
    int L = vel + i;
    int R = vel - i;  
    motors(L,R);
    delay(500);
  }
}

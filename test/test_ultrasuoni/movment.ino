void Rev(int t){
  digitalWrite(28,LOW);
  analogWrite(4,60);

  digitalWrite(24,LOW);
  analogWrite(2,60);

  digitalWrite(26,LOW);
  analogWrite(3,60);
 
  digitalWrite(22,LOW);
  analogWrite(5,60);

  delay(t);
}

void dritto(int t){
  digitalWrite(28,HIGH);
  analogWrite(4,60);

  digitalWrite(24,HIGH);
  analogWrite(2,60);

  digitalWrite(26,HIGH);
  analogWrite(3,60);
 
  digitalWrite(22,HIGH);
  analogWrite(5,60);

  delay(t);
}

void stopped(int t){
  digitalWrite(28,HIGH);
  analogWrite(4,0);

  digitalWrite(24,HIGH);
  analogWrite(2,0);

  digitalWrite(26,HIGH);
  analogWrite(3,0);
 
  digitalWrite(22,HIGH);
  analogWrite(5,0);

  delay(t);
}

void sinistra(int t){
  digitalWrite(28,HIGH);
  analogWrite(4,100);

  digitalWrite(24,HIGH);
  analogWrite(2,100);

  digitalWrite(26,LOW);
  analogWrite(3,100);
 
  digitalWrite(22,LOW);
  analogWrite(5,100);

  delay(t);
}

void destra(int t){
  digitalWrite(28,LOW);
  analogWrite(4,90);

  digitalWrite(24,LOW);
  analogWrite(2,90);

  digitalWrite(26,HIGH);
  analogWrite(3,150);
 
  digitalWrite(22,HIGH);
  analogWrite(5,150);

  delay(t);
}

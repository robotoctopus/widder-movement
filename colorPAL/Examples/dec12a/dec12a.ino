// https://medicaldevices470.wordpress.com/2012/11/02/lab-3-the-colorpal-color-sensor/
// http://be470merchantuy.blogspot.it/2012/02/lab-3-serialread-and-colorpal.html

/*====================================================
/ Connect ColorPAL SIG signal to Arduino pin 2 and 3
/ Add 2K pullup resistor from pins 2 & 3 to +5v
/ Baud Rate = 9600 kbps
/ Read signal on 9600 in HEX
/====================================================*/

#include <SoftwareSerial.h>
#define one 2
#define two 3

SoftwareSerial In(one, two);  // rx = 2, tx = 3
SoftwareSerial Out(two, one);  // rx = 2, tx = 3

float Cred; int Ured; int Kred = 25; int Wred = 367;
float Cgrn; int Ugrn; int Kgrn = 16; int Wgrn = 235;
float Cblu; int Ublu; int Kblu = 31; int Wblu = 461;

int gotcolor = 0;

void setup(){
 Serial.begin(9600); // Start communication with serial port read value
 Color90.begin(4800); // Send signal to led to read value

 /*pinMode(one,INPUT); // serial pin out from color pal
 pinMode(two,INPUT); // from same serial pin, signal pulls up, sends, pulls down, reads
 digitalWrite(one,HIGH); // Enable the pull-up resistor
 digitalWrite(two,HIGH); // Enable the pull-up resistor

 pinMode(one,OUTPUT); // send signal out
 pinMode(two,OUTPUT);
 digitalWrite(one,LOW); // turn pin off so pin 3 can go high
 digitalWrite(two,LOW);

 pinMode(one,INPUT); // Input signal to print
 pinMode(two,INPUT);

 Serial.println("Pass 1");
//  delay(20);

 while(digitalRead(one) != HIGH || digitalRead(two) != HIGH ) {
   Serial.println("In the loop");
   delay(50);
 }

 Serial.println("Pass 2");

 pinMode(one,OUTPUT);
 pinMode(two,OUTPUT);
 digitalWrite(one,LOW);
 digitalWrite(two,LOW);
 delay(150);     // spec is 80, but not all ColorPAL units work with 80

 pinMode(one,INPUT);
 pinMode(two,OUTPUT);
 delay(100);*/

  pinMode(one,OUTPUT);
  pinMode(two,OUTPUT);
  digitalWrite(one,LOW);
  digitalWrite(two,LOW);
  pinMode(one,INPUT); // Input signal to print
  pinMode(two,INPUT);
  while(digitalRead(one) != HIGH || digitalRead(two) != HIGH ) Serial.println("In the loop");
  pinMode(one,OUTPUT);
  pinMode(two,OUTPUT);
  digitalWrite(one,LOW);
  digitalWrite(two,LOW);
  delay(150);
  Color90.print("= (00 $ m) !");  // set up loop to continuously send color data
  pinMode(one,INPUT);
  pinMode(two,OUTPUT);
  delay(100);
}

// This oscillates back and forth on one wire to turn off led, send signal,
// turn on led, read signal. very fast strobe read – arduino is not capable of
// one wire signal communication over digital ports, so this is a way around
// that over 2 wires communicating with 1 pin on the sensor.
//———————————

void loop(){
 readcolor();
 Serial.print("R ");
 Serial.print(Cred);
 Serial.print("   G ");
 Serial.print(Cgrn);
 Serial.print("   B ");
 Serial.println(Cblu); 
 gotcolor = 0;
 delay(100);
}

void readcolor() {  // Reads ColorPAL, putting results in the red,grn,blu variables
  char rByte[9];
  char dummy[4];

  delay(20);
  Color90.begin(4800);
  Color90.print("= (00 $ m) !");  // set up loop to continuously send color data
  pinMode(two,INPUT);
  //gotcolor = 0;
  if (Color90.available() > 0) {
    rByte[0] = Color90.read();
    if( rByte[0] == '$' ) {
      Serial.println("ok1");
      //gotcolor = 1;
      for(int i=0; i<9; i++) {
        rByte[i] = Color90.read();
      }
      Serial.println(" ");
      dummy[0] = rByte[0];
      dummy[1] = rByte[1];
      dummy[2] = rByte[2];
      dummy[3] = 0;

      Ured = strtol(dummy,NULL,16);
      Cred = 255*(Ured - Kred)/(Wred - Kred);

      dummy[0] = rByte[3];
      dummy[1] = rByte[4];
      dummy[2] = rByte[5];

      Ugrn = strtol(dummy,NULL,16);
      Cgrn = 255*(Ugrn - Kgrn)/(Wgrn - Kgrn);

      dummy[0] = rByte[6];
      dummy[1] = rByte[7];
      dummy[2] = rByte[8];

      Ublu = strtol(dummy,NULL,16); 
      Cblu = 255*(Ublu - Kblu)/(Wblu - Kblu);

      define();
    }
    //else{Serial.println("wrong");}
  }
}

void define (){
  float sum = (Cred + Cblu +Cgrn);
  if (sum <= 4) Serial.print("Black");
  else if (sum > 250) Serial.print("White");
  else if (sum > 4 && sum <=250 && Cred > Cblu && Cred > Cgrn) Serial.print("Red");
  else if (sum > 4 && sum <=250 && Cgrn > Cblu && Cgrn > Cred) Serial.print("Green");
  else if (sum > 4 && sum <=250 && Cblu > Cred && Cblu > Cgrn) Serial.print("Blue");
}

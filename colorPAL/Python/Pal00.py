import serial               # per porta seriale
import RPi.GPIO as GPIO     # per pin
import time
import numpy as np
import io                   # dealing with I/O serial

ser = serial.Serial()
unused = serial.Serial()
ser.baudrate = 4800
unused.baudrate = 4800
ser.port = 2
unused.port = 255
sioin = io.TextIOWrapper(io.BufferedRWPair(ser, unused))
siout = io.TextIOWrapper(io.BufferedRWPair(unused, ser))

def setup():
    time.sleep(0.2)
    GPIO.setup(ser, GPIO.OUT)
    GPIO.output(ser, GPIO.LOW)
    GPIO.setup(ser, GPIO.IN)
    while GPIO.input(ser) != HIGH:      # check
        time.sleep(0.1)
    GPIO.setup(ser, GPIO.OUT)
    GPIO.output(ser, GPIO.LOW)
    time.sleep(0.08)
    GPIO.setup(ser, GPIO.IN)
    time.sleep(0.2)

    GPIO.setup(ser, GPIO.OUT)
    siout.write('=(00 $ m)!')

    GPIO.setup(ser, GPIO.IN)

def readcolor():                # Reads ColorPAL, putting results in the red,grn,blu variables
    buf = np.chararray((32))
    
    if sioin is True:
        buf[0] = sioin.readline()
        if rByte[0] == '$':
            for i in range (0,9):
                rByte[i] = sioin.readline()
            parseAndPrint(buf)

def parseAndPrint(buf): #void parseAndPrint(char * data)
    #sscanf (data, "%3x%3x%3x", &Cred, &Cgrn, &Cblu)
    buf = np.chararray((48))
    #sprintf(buf, "r = %4.4d    g = %4.4d    b = %4.4d", Cred, Cgrn, Cblu);
    print buf
    
    if Cgrn > 200 and Cgrn > Cblu and Cgrn > Cred:      #valori ok
        verde = True
    elif Cgrn > 200 and Cblu > 200 and Cred > 200:      #valori no buoni
        argento = True

while True:             #per ora
    if verde == False and argento == False:
        #seguilinea
    elif verde == True:
        #Arduino curva
    else:
        #pallina

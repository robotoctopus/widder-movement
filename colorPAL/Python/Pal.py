# -*- coding: cp1252 -*-
# https://medicaldevices470.wordpress.com/2012/11/02/lab-3-the-colorpal-color-sensor/

#Serial port i/o -> pyserial (https://pypi.python.org/pypi/pyserial/2.7)

'''====================================================
Connect ColorPAL SIG signal to Arduino pin 2 and 3
Add 2K pullup resistor from pins 2 & 3 to +5v
(see image attached)

Baud Rate = 9600 kbps
Read signal on 9600 in HEX
====================================================='''

import serial
import RPi.GPIO as GPIO     #per pin
import time
import numpy as np
import io

Kred = 25
Wred = 367
Kgrn = 16
Wgrn = 235
Kblu = 31
Wblu = 461

ser = serial.Serial()
ser.baudrate = 4800
ser.port = 'COM1'
sio = io.TextIOWrapper(io.BufferedRWPair(ser, ser))

GPIO.setup(2, GPIO.IN)          # serial pin out from color pal
GPIO.setup(3, GPIO.IN)          # from same serial pin, signal pulls up, sends, pulls down, reads
GPIO.output(2, GPIO.HIGH)       # Enable the pull-up resistor
GPIO.output(3, GPIO.HIGH)       # Enable the pull-up resistor

GPIO.setup(2, GPIO.OUT)         # send signal out
GPIO.setup(3, GPIO.OUT)
GPIO.output(2, GPIO.LOW)        # turn pin off so pin 3 can go high
GPIO.output(3, GPIO.LOW)

GPIO.setup(2, GPIO.IN)          #Input signal to print
GPIO.setup(3, GPIO.IN)

while GPIO.input(2) is False or GPIO.input(3) is False:
    time.sleep(0.1)

GPIO.setup(2, GPIO.OUT)
GPIO.setup(3, GPIO.OUT)
GPIO.output(2, GPIO.LOW)
GPIO.output(3, GPIO.LOW)
time.sleep(0.15)
GPIO.setup(2, GPIO.IN)
GPIO.setup(3, GPIO.IN)
time.sleep(0.1)

def readcolor():                # Reads ColorPAL, putting results in the red,grn,blu variables
    rByte = np.chararray((9))
    dummy = np.zeros((4))
    ser.write('= (00 $ m) !')   # set up loop to continuously send color data
    GPIO.setup(3, GPIO.IN)
    gotColor = 0
    while gotColor == 0:
        rByte[0] = sio.readline()
        if rByte[0] != 0:
            gotColor = 1
            for i in range (0,9):
                rByte[i] = sio.readline()
        #red
            dummy[0] = int(rByte[0],16)                     # colorPAL sends hexadecimal datas (from '0' to 'ff')
            dummy[1] = int(rByte[1],16)
            dummy[2] = int(rByte[2],16)
            dummy[3] = 0
            Ured = np.mean(dummy[0:3])                      # just one data required
            Cred = float(255*(Ured - Kred)/(Wred - Kred))   # tip from Parallax documentation

        #green
            dummy[0] = int(rByte[0],16)
            dummy[1] = int(rByte[1],16)
            dummy[2] = int(rByte[2],16)
            Ugrn = np.mean(dummy[0:3])
            Cgrn = float(255*(Ugrn - Kgrn)/(Wgrn - Kgrn))

        #blue
            dummy[0] = int(rByte[0],16)
            dummy[1] = int(rByte[1],16)
            dummy[2] = int(rByte[2],16)
            Ublu = np.mean(dummy[0:3])
            Cblu = float(255*(Ublu - Kblu)/(Wblu - Kblu))

            if Cgrn > 200 and Cgrn > Cblu and Cgrn > Cred:      #valori ok
                verde = True
            elif Cgrn > 200 and Cblu > 200 and Cred > 200:      #valori no buoni
                argento = True

while True:             #per ora
    if verde == False and argento == False:
        #seguilinea
    elif verde == True:
        #Arduino curva
    else:
        #pallina

#include <SoftwareSerial.h>
#define sio  13            // ColorPAL connected to pin 2
#define unused  256       // Non-existant pin # for SoftwareSerial
#define sioBaud  4800

int greenPAL ;

SoftwareSerial serin(sio, unused);
SoftwareSerial serout(unused, sio);

void PALsetup() {
  
 reset();          // Send reset to ColorPal
 serout.begin(sioBaud);
 pinMode(sio, OUTPUT);
 serout.print("= (00 $ m) !"); // Loop print values, see ColorPAL documentation
 serout.end();      // Discontinue serial port for transmitting  
 serin.begin(sioBaud);         // Set up serial port for receiving
 pinMode(sio, INPUT); 
 Serial.println("setup effetuato");
 
}

void reset() {
 delay(200);
 pinMode(sio, OUTPUT);
 digitalWrite(sio, LOW);
 pinMode(sio, INPUT);
 while (digitalRead(sio) != HIGH);
 pinMode(sio, OUTPUT);
 digitalWrite(sio, LOW);
 delay(80);
 pinMode(sio, INPUT);
 delay(200);
}


int PALloop() { 
  
  char stringa[32];
  int red;  
  int grn;  
  int blu;

  float redF;  
  float grnF;
  float bluF;
 
   serin.listen();  
   
   if (serin.available() > 0) {
   // Wait for a $ character, then read three 3 digit hex numbers   
   
   stringa[0] = serin.read();
   if (stringa[0] == '$') {
     for(int i = 0; i < 9; i++) {
       while (serin.available() == 0);     // Wait for next input character
       stringa[i] = serin.read();
        if (stringa[i] == '$')               // Return early if $ character encountered
        return;
      }
      delay(10);   
   }
      sscanf (stringa, "%3x%3x%3x", &red, &grn, &blu);
      char colred[32];  // RIDURRE
      char colblu[32];
      char colgrn[32];
      sprintf(colred, "%4.4d ",red);
      sprintf(colblu, "%4.4d ",blu);
      sprintf(colgrn, "%4.4d ",grn);
     

      String redS = colred;
      redF = redS.toFloat();
      
      String grnS = colgrn;
       grnF = grnS.toFloat();
      
      String bluS = colblu;
      bluF = bluS.toFloat();
      
     Serial.println("sensori =");
      Serial.println(redF);
      Serial.println(grnF);
      Serial.println(bluF); 
      
 }

if(  grnF < 60.0 && grnF > 40.0 && bluF < 60.0 && bluF > 30.0) {
  greenPAL = greenPAL + 1;
 }
 else {
 greenPAL = 0;
 }

}




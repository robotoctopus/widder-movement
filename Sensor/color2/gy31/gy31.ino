#define s0  8
#define s1 9  
#define s2 10 
#define s3 11  
#define out 12



void GYsetup()   
{   
 
  pinMode(s0, OUTPUT);  
  pinMode(s1, OUTPUT);  
  pinMode(s2, OUTPUT);  
  pinMode(s3, OUTPUT);  
  pinMode(out, INPUT);   
  
  digitalWrite(s0, HIGH);  
  digitalWrite(s1, LOW);  
} 
 
int GYloop() {   
 int red;  
 int green;  
 int blue;  
 int greenGY = 0;
      
  digitalWrite(s2, LOW);  // for green
  digitalWrite(s3, LOW);  //
  red = pulseIn(out, digitalRead(out) == HIGH ? LOW : HIGH);  
  digitalWrite(s3, HIGH);  
  blue = pulseIn(out, digitalRead(out) == HIGH ? LOW : HIGH);  
  digitalWrite(s2, HIGH);  
  green = pulseIn(out, digitalRead(out) == HIGH ? LOW : HIGH); 

     
  if(red < 155 && green < 95 && blue < 95 && red > 120 && green > 80 && blue > 80 ) greenGY = 1;
 

 /*Serial.println("RED");  
  Serial.println(red, DEC);  
  Serial.println(" GREEN ");  
  Serial.println(green, DEC);  
  Serial.println(" BLUE ");  
  Serial.println(blue, DEC);  
  Serial.println("____________________________ "); */

   return greenGY;

}
 

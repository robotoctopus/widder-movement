

const int numSens = 8;
const int timeOut = 2500;
const int pinEmitter = 33;

QTRSensorsRC qtrrc((unsigned char[]) {53, 51, 49, 47, 43, 41, 39, 37}, numSens, timeOut, pinEmitter);
unsigned int sensorValues[8];


void calibration() {
  for (int i = 0; i < 400; i++) {
    qtrrc.calibrate();
  }
}

void  Black_ops() {
  qtrrc.readCalibrated(sensorValues);
  for(int i = 0; i < 8; i++){
    Serial.println(sensorValues[i]);
  }
  Serial.println(" ");
  delay(500);
}

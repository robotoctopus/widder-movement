#include <SoftwareSerial.h>

const int sio = 11;            // ColorPAL connected to pin 2
const int unused = 256;        // Non-existant pin # for SoftwareSerial
const int sioBaud = 4800;

int red;
int grn;
int blu;// Set up two software serials on the same pin.

SoftwareSerial serin(sio, unused);
SoftwareSerial serout(unused, sio);

void setup() {
  
Serial.begin(9600);
Serial.print("hello");
 reset();          // Send reset to ColorPal
 serout.begin(sioBaud);
 pinMode(sio, OUTPUT);
 serout.print("= (00 $ m) !"); // Loop print values, see ColorPAL documentation
 serout.end();      // Discontinue serial port for transmitting  
 serin.begin(sioBaud);         // Set up serial port for receiving
 pinMode(sio, INPUT); 
 Serial.println("setup effetuato");
 
}

void reset() {
 delay(200);
 pinMode(sio, OUTPUT);
 digitalWrite(sio, LOW);
 pinMode(sio, INPUT);
 while (digitalRead(sio) != HIGH);
 pinMode(sio, OUTPUT);
 digitalWrite(sio, LOW);
 delay(80);
 pinMode(sio, INPUT);
 delay(200);
}

void loop() {
char stringa [32];

readData(stringa); 
Serial.print("sensore = ");
Serial.println(stringa);
delay(10); 
 } 



void readData(char * stringa) {
 
   serin.listen();  
   
   if (serin.available() > 0) {
   // Wait for a $ character, then read three 3 digit hex numbers   
   
   stringa[0] = serin.read();
   if (stringa[0] == '$') {
     for(int i = 0; i < 9; i++) {
       while (serin.available() == 0);     // Wait for next input character
       stringa[i] = serin.read();
        if (stringa[i] == '$')               // Return early if $ character encountered
        return;
      }
   }
 }
}


/*void Conversione2(char * data) {

 Serial.println("CALCOLO2");
 sscanf (data, "%3x%3x%3x", &red, &grn, &blu);
 char buffer[32];
 sprintf(buffer, " red2 =%4.4d ",red);
 Serial.println(buffer);
}*/
   

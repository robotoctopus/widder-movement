/* ColorPal Sensor Example for Arduino
  Author: Martin Heermance, with some assistance from Gordon McComb
  This program drives the Parallax ColorPAL color sensor and provides
  serial RGB data in a format compatible with the PC-hosted 
  TCS230_ColorPAL_match.exe color matching program.
*/
 
//#include <Serial.h>
#define Pserin  17
#define Pserout  16     // ColorPAL connected to pin 2
#define Pserin2  4
#define Pserout2  3  
const int unused = 255;     // Non-existant pin # for SoftwareSerial
const int sioBaud = 4800;
const int waitDelay = 200;
 
// Received RGB values from ColorPAL
int red;
int grn;
int blu;
 
void setup() {
 pinMode(Pserout, OUTPUT); 
 pinMode(Pserin,INPUT);
 Serial.begin(9600);
   Serial.println("prereset");
  reset();          // Send reset to ColorPal
  Serial.println("reset");
  Serial2.begin(sioBaud);
  //pinMode(sio, OUTPUT);
  Serial2.print("= (00 $ m) !"); // Loop print values, see ColorPAL documentation
  Serial2.end();        // Discontinue serial port for transmitting
  //serin.begin(sioBaud);          // Set up serial port for receiving
  //pinMode(sio, INPUT);
 
 /*reset();          // Send reset to ColorPal
  Serial3.begin(sioBaud);
  //pinMode(sio, OUTPUT);
  Serial3.print("= (00 $ m) !"); // Loop print values, see ColorPAL documentation
  Serial3.end();        // Discontinue serial port for transmitting
  //serin.begin(sioBaud);          // Set up serial port for receiving
  //pinMode(sio, INPUT);*/
}
 
void loop() {
  readData();  
  
 // serin.end();  
 // readData2();
 
 // serin2.end();
 
}  
 
// Reset ColorPAL
void reset() {
  delay(200);
  //pinMode(sio, OUTPUT);
  digitalWrite(Pserout, LOW);
  //pinMode(sio, INPUT);
  Serial.println("prehigh");
  while (Pserin != HIGH);
  
  //pinMode(sio, OUTPUT);
  digitalWrite(Pserout, LOW);
  delay(80);
  
  //pinMode(sio, INPUT);
  delay(waitDelay);
}
 
 
void reset2() {
  delay(200);
  //pinMode(sio, OUTPUT);
  digitalWrite(Pserout2, LOW);
  //pinMode(sio, INPUT);
  while (Pserin2 != HIGH);
  //pinMode(sio, OUTPUT);
  digitalWrite(Pserout2, LOW);
  delay(80);
  //pinMode(sio, INPUT);
  delay(waitDelay);
}
 
void readData() {
  char buffer[32];
 
   // serin.listen();
 
  if (Serial2.available() > 0) {
    // Wait for a $ character, then read three 3 digit hex numbers

    buffer[0] = Serial2.read();
    if (buffer[0] == '$') {
      for(int i = 0; i < 9; i++) {
        while (Serial2.available() == 0);     // Wait for next input character
        buffer[i] = Serial2.read();
        Serial.println(i);
        if (buffer[i] == '$')           // Return early if $ character encountered      
        return;    
      }
   
      Conversione(buffer);
      delay(10);
    }
  }
}
 
void readData2() {
  char buffer[32];
  
   // serin2.listen();
 
  if (Serial3.available() > 0) {
    // Wait for a $ character, then read three 3 digit hex numbers
 
    buffer[0] = Serial3.read();
    if (buffer[0] == '$') {
      for(int i = 0; i < 9; i++) {
        while (Serial3.available() == 0);     // Wait for next input character
        buffer[i] = Serial3.read();
        if (buffer[i] == '$')               // Return early if $ character encountered
        return;
      }
      
      Conversione2(buffer);
      delay(10);
    }
  }
}
 
void Conversione(char * data) {
   Serial.println("start ");
  sscanf (data, "%3x%3x%3x", &red, &grn, &blu);
  char buffer[32];
  sprintf(buffer, "red =%4.4d ",red);
  Serial.println(buffer);
}
 
 
void Conversione2(char * data) {
  Serial.println("start 2");
  sscanf (data, "%3x%3x%3x", &red, &grn, &blu);
  char buffer[32];
  sprintf(buffer, " red2 =%4.4d ",red);
  Serial.println(buffer);
}


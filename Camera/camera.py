import smbus
import math
import cv2
import time
import numpy as np

bus = smbus.SMBus(1)
address = 0x04
cap = cv2.VideoCapture(0)
lower = np.array([0,0,0])
upper = np.array([50,50,50])

class Communication:
    def write (self,value):              #to send commands
        bus.write_byte(address, value)
        return -1

    def read (self):                     #to recive data
        number = bus.read_byte(address)
        return number

class Setup:                                #initialize all
    def capture(self):                      #take the camera image
        ret, original = cap.read()          # x: 480; y: 640
        #img = original[20:480, 230:410]     #to check by tests
        return img

    def b_frame(self):                      #prepare the image to find balls
        img = camera.capture()
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = cv2.equalizeHist(img)
        img = cv2.convertScaleAbs(img, 40, 1.4)
        return img

    def t_frame(self):                      #prepare the image to find the triangle
        original = camera.capture()
        hsv = cv2.cvtColor(original, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(hsv, lower, upper)
        return mask

class LookFor(Setup):                       #to look for ball/triangle
    def b_find(self):
        img = camera.b_frame()
        circles = cv2.HoughCircles(img, cv2.cv.CV_HOUGH_GRADIENT, 1, minDist = 2, param1=160, param2=55, minRadius=15,
                                   maxRadius=75)        #valori per argento e nero diversi!!!
        if circles is None:
            i2c.write(90)            # should move right or left to look for other balls (90 degrees -> CHECK)
            result = camera.b_centre()       # look for other balls in this new space
        else:
            X = np.zeros((0))  # create an empty array
            Y = np.zeros((0))
            for i in circles[0, :]:
                X = np.append(X, i[0])              # add to the empty array all circles' centre x
                Y = np.append(Y, i[1])
            x = int(np.mean(X))                     # calculate the average x -> !!!problems if there are more circles
            y = int(np.mean(Y))
            result = np.array([x,y])
        return result
            
    def t_find(self):
        k = 0
        for i in range(0,4):                #look for the triangle in 360 -> !!! be sure all the pitch is seen
            mask = camera.t_frame()
        # find all black shapes
            (contours, _) = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            for c in contours:
                area = cv2.contourArea(c)
                if area > k:                #find the biggest black shape
                    k = area
                    big = c
                    times = i
            i2c.write(90)                   #360/4
        if k != 0:
            i2c.write(90*i)
            M = cv2.moments(big)
            x = int(M["m10"] / M["m00"])    #find the triangle's centre
            y = int(M["m01"] / M["m00"])
            result = np.array([x, y])
        else:
            i2c.write(30)
            i2c.write(0.5)
            result = camera.t_centre()
        return result

class Guide(LookFor):
    def angle(self,r):  #to improve
        x = r[0] - 320                                           #move the origin from top left to bottom middle
        y = abs(r[1] - 479)                                 #dipends on the dimentions of the frame
        angle = 90 - int(round(math.degrees(math.atan2(y, x))))      #find the direction; atan2(y,x) = atan(y/x), but x can be equal to 0
        i2c.write(alpha)                                        #Arduino turns alpha degrees

    def b_centre(self):
        r = camera.b_find()
        camera.angle(r)         #robot starts turning       
        number = i2c.read()     #Arduino tells Raspberry time it takes to turn
        time.sleep(number)      #wait for number seconds
        camera.Go()

    def t_centre(self):         #idem as above
        r = camera.t_find()
        camera.angle(r)
        number = i2c.read()
        time.sleep(number)
        camera.Go()

    def Go(self):
        number = i2c.read()     #Arduino tells Raspberry when it reaches the ball/triangle
        while number != 7:                      # if not ball/triangle reached yet
            i2c.write(0)     

camera = Guide()
i2c = Communication()

for i in range(0,3):            #3 is the number of the balls (CAN CHANGE) ->   target: do this untill you find non more balls
    camera.b_centre()       #if it has to take-bring, camera.t_centre inside the for loop
camera.t_centre()
#now should go out -> Arduino's duty

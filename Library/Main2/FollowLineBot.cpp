#include "Arduino.h"
#include "FollowLineBot.h"

//PIN SETS CLASS ----------------------------------------------------------------------------------------

//initializer
Pins::Pins(int a, int b) { pa = a; pb = b; }
Pins::Pins(const Pins& pin) { pa = pin.pE(); pb = pin.pP(); }

int Pins::pE() { return pa;}
int Pins::pP() { return pb;}

//DIRECTION CLASSes (Top, Bottom, Left, Right, middle) -------------------------------------------------------------

//initializer
Directions2D::Directions2D (char a, char b) { x = a; y = b; }
Directions2D::Directions2D () {}

//Stock directions
Directions2D Directions2D::TopRight()      { return Directions2D('t','r'); }

Directions2D Directions2D::TopLeft()       { return Directions2D('t','l'); }

Directions2D Directions2D::TopMiddle()     { return Directions2D('t','m'); }

Directions2D Directions2D::BottomRight()   { return Directions2D('b','r'); }

Directions2D Directions2D::BottomLeft()    { return Directions2D('b','l'); }

Directions2D Directions2D::BottomMiddle()  { return Directions2D('b','m'); }

Directions2D Directions2D::MiddleRight()   { return Directions2D('m','r'); }

Directions2D Directions2D::MiddleLeft()    { return Directions2D('m','l'); }

Directions2D Directions2D::Centre()        { return Directions2D('m','m'); }

//From a 2D direction to a 1D direction
char Directions2D::to1D(char c) {
  if (c == x) {
    return y;
  } else if (c == y) {
    return x;
  }
}

//MOTOR CLASS --------------------------------------------------------------------------------------------

//initializer
Motor::Motor(Pins pins, Directions2D posi)  {
    pe = pins.pE();
    pp = pins.pP();
    pinMode(pe,OUTPUT);
    pinMode(pp,OUTPUT);
    pos = posi; 
    HorizontalPosition = posi.horizontal();
    VerticalPosition = posi.vertical();
}

Motor::Motor(const Motor& mot)  {
    Pins pin(mot.GetPins());
    pe = pin.pE();
    pp = pin.pP();
    Directions2D posPlaceHolder(mot.HorizontalPosition, mot.VerticalPosition);
    pos = posPlaceHolder; 
    HorizontalPosition = posPlaceHolder.horizontal();
    VerticalPosition = posPlaceHolder.vertical();
}

Motor::Motor() {}

void Motor::motorSetup() {
    pinMode(pe,OUTPUT);
    pinMode(pp,OUTPUT);
}

//Set the motor to go forward
void Motor::GoFwd(int vel) {digitalWrite(pp, HIGH); analogWrite(pe, vel); }

//Set the motor to go backward
void Motor::GoRev(int vel) {digitalWrite(pp, LOW); analogWrite(pe, vel); }

//Stops the motor
void Motor::Stop() {digitalWrite(pp, LOW); analogWrite(pe, 0); }

//AARRAY OF MOTORS CLASS --------------------------------------------------------------------------------

//Initializer
MotorsArr::MotorsArr(Motor mot[]) {
  Array[4] = mot[4];
}

MotorsArr::MotorsArr() {}

void MotorsArr::motorsSetup() {
  for (int i = 0; i < 4; i++) {
    Array[i].motorSetup();
  }
}

MotorsArr::FindIndexOf(Directions2D dir) {
  for (int i = 0; i < 4; i++) {
    if (Array[i].Position() == dir) {
      return i; 
    }
  }
}

//ROBOT CLASS (MAIN) ------------------------------------------------------------------------------------

//initializer
FollowLineBot::FollowLineBot(MotorsArr m, int v) {
      motors = m; 
      velocity = v;
}

void FollowLineBot::botup() {
  motors.motorsSetup();
}

Motor FollowLineBot::getMotor(Directions2D dir) {
  int index = motors.FindIndexOf(dir);
  return motors.Array[index];
}

Motor FollowLineBot::getMotor(int index) {
  return motors.Array[index];
}

void FollowLineBot::onFwd() {
  for (int i = 0; i < 4; i++) {
    Motor mot(motors.Array[i]);
    mot.GoFwd(velocity);
  }
}

void FollowLineBot::onFwd(int v) {
  for (int i = 0; i < 4; i++) {
    Motor mot(motors.Array[i]);
    mot.GoFwd(v);
  }
}

void FollowLineBot::onRev() {
  for (int i = 0; i < 4; i++) {
    Motor mot(motors.Array[i]);
    mot.GoRev(velocity);
  }
}

void FollowLineBot::stopAll() {
  for (int i = 0; i < 4; i++) {
    Motor mot(motors.Array[i]);
    mot.Stop();
  }
}

void FollowLineBot::onTurnSmooth(char d, float p) {
  for (int i = 0; i < 4; i++) {
    Motor mot(motors.Array[i]);
    if (mot.HorizontalPosition == d) {
      mot.GoFwd(velocity * p);
    } else {
      mot.GoFwd(velocity);
    }
  }
}

void FollowLineBot::onTurn90(char d) {
  for (int i = 0; i < 4; i++) {
    Motor mot(motors.Array[i]);
    if (mot.HorizontalPosition == d) {
      mot.GoRev(velocity);
    } else {
      mot.GoFwd(velocity);
    }
  }
}


//onFwd e company con delay
//onFwd e company con vel selezionabile
//onFwd e company con sia vel che delay personalizzabli


//Precode ----------------------------------------------------------------------------------------------------------------------------------------------------------------
//Includes

  #include"FollowLineBot.h"
  
//Class declaration
  //Motor pins declaration:
  
      Pins pin1(1,1);  //<---
      Pins pin2(1,1);  //<---
      Pins pin3(1,1);  //<---
      Pins pin4(1,1);  //<---
    
  //Motor declaration:
  
      Directions2D  dir1('t','l');   //<---
      Directions2D  dir2('t','l');   //<---
      Directions2D  dir3('t','l');   //<---
      Directions2D  dir4('t','l');   //<---
      Motor mot1(pin1, dir1);
      Motor mot2(pin2, dir2);
      Motor mot3(pin3, dir3);
      Motor mot4(pin4, dir4);

      Motor arr[4] = {mot1, mot2, mot3, mot4};
    
  //Motor Array declaration:
  
      MotorsArr motors(arr);
    
  //Follow Line bot declaration:
  
      FollowLineBot bot(motors,1); 

//Actual code ----------------------------------------------------------------------------------------------------------------------------------------------------------
void setup() {
  // put your setup code here, to run once:
}

void loop() {
  // put your main code here, to run repeatedly:

}

#include "arduino.h"

//PIN SETS CLASS ----------------------------------------------------------------------------------------

class Pins {
  public:
    Pins(int, int);
    int pE () {return pa;}
    int pP () {return pb;}
  private:
    int pa;
    int pb;
};

//initializer
Pins::Pins(int a, int b) { pa = a; pb = b; }

//DIRECTION CLASS (Top, Bottom, Left, Right) -------------------------------------------------------------

class Directions {
  public:
    Directions (char,char);
    char vertical () {return dv;}
    char horizontal () {return dh;}
    Directions TopRight();
    Directions TopLeft();
    Directions BottomRight();
    Directions BottomLeft();
    char Right();
    char Left();
  private:
    char dv;
    char dh;
};

//initializer
Directions::Directions (char a,char b) { dv = a; dh = b; }

//Stock directions
Directions Directions::TopRight() {
    return Directions('t','r');  
}

Directions Directions::TopLeft() {
    return Directions('t','l');  
}

Directions Directions::BottomRight() {
    return Directions('b','r');  
}

Directions Directions::BottomLeft() {
    return Directions('b','l');  
}

char Directions::Right() {
    return 'r';  
}

char Directions::Left() {
    return 'l';  
}

//MOTOR CLASS --------------------------------------------------------------------------------------------

class Motor {
  public:
    Motor(Pins, Directions);
    //void GetPins() {int pins[2] = {pe, pp}; return pins[];}
    void GoFwd(int);
    void GoRev(int);
    void Stop();
    Directions Position() {return pos;}
    char HorizontalPosition;
    char VerticalPosition;
  private:
    int pe;
    int pp;
    Directions pos;
};

//initializer
Motor::Motor(Pins pins, Directions posi) { 
    pe = pins.pE;
    pp = pins.pP;
    pinMode(pe,OUTPUT);
    pinMode(pp,OUTPUT);
    pos = posi; 
    HorizontalPosition = posi.horizontal;
    VerticalPosition = posi.vertical;
}

//Set the motor to go forward
void Motor::GoFwd(int vel) {digitalWrite(pp, HIGH); analogWrite(pe, vel); }

//Set the motor to go backward
void Motor::GoRev(int vel) {digitalWrite(pp, LOW); analogWrite(pe, vel); }

//Stops the motor
void Motor::Stop() {digitalWrite(pp, LOW); analogWrite(pe, 0); }

//ROBOT CLASS (MAIN) ------------------------------------------------------------------------------------

class FollowLineBot {
    public:
      FollowLineBot(Motor);
        //Motors functions
          void onFwd();
          void onRev();
          void stopAll();
          void onTurnSmooth(char, float);
          void onTurn90(char);
    private:
      //motors variables
        Motor motors[];
        int velocity;
};

//initializer
FollowLineBot::FollowLineBot (Motor m[], int v) {
      motors[] = m[]; 
      velocity = v;
}

void FollowLineBot::onFwd() {
  for (int i = 0; i < motors[].length; i++) {
    motors[i].GoFwd(velocity);
  }
}

void FollowLineBot::onRev() {
  for (int i = 0; i < motors[].length; i++) {
    motors[i].GoRev(velocity);
  }
}

void FollowLineBot::stopAll() {
  for (int i = 0; i < motors[].length; i++) {
    motors[i].Stop();
  }
}

void FollowLineBot::onTurnSmooth(char d, float p) {
  for (int i = 0; i < motors[].length; i++) {
    if (motors[i].HorizontalPosition == d) {
      motors[i].GoFwd(velocity * p);
    } else {
      motors[i].GoFwd(velocity);
    }
  }
}

void FollowLineBot::onTurn90(char d) {
  for (int i = 0; i < motors[].length; i++) {
    if (motors[i].HorizontalPosition == d) {
      motors[i].GoRev(velocity);
    } else {
      motors[i].GoFwd(velocity);
    }
  }
}


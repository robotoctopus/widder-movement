#ifndef FollowLineBot_h
#define FollowLineBot_h

#include "Arduino.h"

//PIN SETS CLASS ----------------------------------------------------------------------------------------

class Pins {               
  public:
    Pins(int, int);
    Pins(const Pins&);                        
    Pins();
    int pE();                                       // pinEnable 
    int pP();                                       // pinPhase
  private:
    int pa;                                         // placeholder A => Public pE
    int pb;                                         // placeholder B => Public pP
};

class Directions2D {                                // semplification for CHAR SYSTEM
  public:
    Directions2D(char,char);                        //Constructor
    Directions2D();                                 //Blank constructor
    bool operator==(const Directions2D& b)          //Operator == to compare directions
    {
      if (this->vertical()==b.vertical() && 
          this->horizontal()==b.horizontal())
      { return true;  } 
      else
      { return false; }  
    };
    char vertical() {return y;}                     //Function to get the vertical char
    char horizontal() {return x;}                   //Function to get the horizontal char 
    Directions2D TopRight();                        //Prefabbricated for TOP RIGHT direction/position
    Directions2D TopLeft();                         //Prefabbricated for TOP LEFT direction/position
    Directions2D TopMiddle();                       //Prefabbricated for TOP MIDDLE direction/position
    Directions2D BottomRight();                     //Prefabbricated for BOTTOM RIGHT direction/position
    Directions2D BottomLeft();                      //Prefabbricated for BOTTOM LEFT direction/position
    Directions2D BottomMiddle();                    //Prefabbricated for BOTTOM MIDDLE direction/position
    Directions2D MiddleRight();                     //Prefabbricated for MIDDLE RIGHT direction/position
    Directions2D MiddleLeft();                      //Prefabbricated for MIDDLE LEFT direction/position
    Directions2D Centre();                          //Prefabbricated for CENTRE direction/position
    char to1D(char);                                //Function to transform a 2D in to 1D direction/position
  private:
    char x;
    char y;
};

class Motor {
  public:
    Motor(Pins, Directions2D);
    Motor(const Motor&);
    Motor();
    void motorSetup();
    int GetPE() {return pe;}
    int GetPP() {return pp;}
    void GoFwd(int);
    void GoRev(int);
    void Stop();
    Directions2D Position() {return pos;}
    char HorizontalPosition;
    char VerticalPosition;
  private:
    const int& pe;
    const int& pp;
    int unicope(){
      return pe;
    }
    int unicopp(){
      return pp;
    }
    Directions2D pos;
};

class MotorsArr {
  public:
    MotorsArr(Motor[4]);
    MotorsArr();
    Motor Array[4];
    void motorsSetup();
    int FindIndexOf(Directions2D);
};

class FollowLineBot {
    public:
      FollowLineBot(MotorsArr,int);
        //General setup
          void botup();
        //Motors functions
          Motor getMotor(Directions2D);
          Motor getMotor(int);
          void onFwd();
          void onFwd(int);
          void onRev();
          void stopAll();
          void onTurnSmooth(char, float);
          void onTurn90(char);
    private:
      //motors variables
        MotorsArr motors;
        int velocity;
};

#endif

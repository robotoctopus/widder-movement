//onFwd e company con delay
//onFwd e company con vel selezionabile
//onFwd e company con sia vel che delay personalizzabli


//Precode ----------------------------------------------------------------------------------------------------------------------------------------------------------------
//Includes

  #include"FollowLineBot.h"
  
//Class declaration
  //Motor pins declaration:
  
      Pins pin1(4,28);  //<---
      Pins pin2(3,26);  //<---
      Pins pin3(2,24);  //<---
      Pins pin4(5,22);  //<---
    
  //Motor declaration:
  
      Directions2D  dir1('t','l');   //<---
      Directions2D  dir2('t','r');   //<---
      Directions2D  dir3('b','r');   //<---
      Directions2D  dir4('b','l');   //<---
      Motor mot1(pin1, dir1);
      Motor mot2(pin2, dir2);
      Motor mot3(pin3, dir3);
      Motor mot4(pin4, dir4);

      Motor arr[4] = {mot1, mot2, mot3, mot4};
    
  //Motor Array declaration:
  
      MotorsArr motors(arr);
    
  //Follow Line bot declaration:
  
      FollowLineBot bot(motors,200); 

//Actual code ----------------------------------------------------------------------------------------------------------------------------------------------------------
void setup() {
  Serial.begin(9600);
   

  bot.botup();
  
}

void loop() {


      Serial.println ("Valore in Pins: ");
    int n = pin1.pE();
    Serial.println(pin1.pE());
    Serial.println(n);
    int x = pin1.pP();
    Serial.println(pin1.pP());
    Serial.println(x);
    Serial.println();
    Serial.println("Value in Motors");
    Serial.println(mot1.GetPE());
    Serial.println(mot1.GetPP());
      bot.onFwd();
  delay(10000);
  // put your main code here, to run repeatedly:

}
